## 18 November 2018 - v1.0.4 ##
* Tidy up

## 11 April 2018 - v1.0.3 ##
* Change location child theme

## 10 April 2018 - v1.0.2 ##
* Fix bug style on fontawesome

## 21 March 2018 - v1.0.1 ##
* Add advance custom field code to themes

## 1 March 2018 - v1.0.0 ##
* Initial Release
