<div class="section full-width home-testimonial">
  <div class="container">
    <div class="title-section">
        <h2><?php the_sub_field('title'); ?></h2>
        <h3><?php the_sub_field('title_2'); ?></h3>
    </div>
  </div>
  <div class="bg-full-width">
  <?php
    $args = array(
      'post_type' => 'hotel-info',
      'category_name' => 'testimonial'
      );
    query_posts($args);
    
    if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php if ( has_post_thumbnail() ) : ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
    <?php endif; ?>
    <div class="overlay"></div>
    <div class="container">
      <div class="wrapper-text">
        <div class="text">
          <div class="outer-text">
            <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>
      <?php endwhile; ?>
      <?php endif; ?>
      <?php wp_reset_query(); ?>
  </div>
</div>