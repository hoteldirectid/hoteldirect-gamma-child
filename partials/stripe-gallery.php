<div class="section home-gallery-grid">
    <div class="container">
        <div class="title-section">
            <h2><?php the_sub_field('title'); ?></h2>
            <h3><?php the_sub_field('title_2'); ?></h3>
        </div>
        <div class="outer-gallery">
            <?php 
            $galleries = get_sub_field('gallery');

            foreach ($galleries as $gallery) : ?>
                <div class="list-gallery">
                    <div class="outer-gallery">
                        <img src="<?php echo $gallery['sizes']['gallery-slide']; ?>" alt="">
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>