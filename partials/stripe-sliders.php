<?php 
$images = get_field('sliders'); 
if($images): ?>    
    <div class="section main-slider">
      <?php do_shortcode("[booking_engine]"); ?>
      <div id="slider-main" class="owl-carousel">
        <?php 
            foreach ($images as $image) {
                echo '<div class="owl-slide" style="background-image: url(\''.$image['url'].'\')"></div>';
            }
        ?>
      </div><!-- end .slider-main -->
    </div><!-- end .main-slider -->
<?php endif; ?>