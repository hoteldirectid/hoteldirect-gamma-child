<div class="section full-width">
    <?php $text_sliders = get_sub_field('slider_text');

    foreach ($text_sliders as $text_slider) : ?>
        
      <div class="bg-full-width">
        <img src="<?php echo $text_slider['image']['sizes']['gallery-slide-main']; ?>" alt="">
        <div class="overlay"></div>
        <div class="container">
            <div class="wrapper-text">
                <div class="text">
                    <div class="outer-text">
                        <h2><?php echo $text_slider['title']; ?></h2>
                        <p>
                            <?php echo $text_slider['description']; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>      


    <?php endforeach; ?>

</div>

