<div class="section home-intro">
    <div class="container">
    <div class="title-section">
        <h2><?php the_sub_field('title'); ?></h2>
        <h3><?php the_sub_field('title_2'); ?></h3>
    </div>
    <div class="outer-home-intro">
        <div class="left-img">
        <div class="outer-img">
            <img src="asset/img/slide/5.jpg" alt="">
        </div>
        </div>
        <div class="text">
        <div class="outer-text">
            <h2>Best Hotel In Yogyakarta City</h2>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
        </div>
        </div>
    </div>
    </div>
</div>