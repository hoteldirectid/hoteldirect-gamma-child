<div class="section home-contact">
    <div class="container">
    <div class="title-section">
        <h2><?php the_sub_field('title'); ?></h2>
        <h3><?php the_sub_field('title_2'); ?></h3>
    </div>
    <div class="outer-home-contact">
        <div class="list-contact">
        <div class="img">
            <div class="outer-img">
            <?php $images = get_sub_field('image');
            
            ?>
                <img src="<?php echo $images['url']; ?>" alt="">
            </div>
        </div>
        <div class="text">
            <div class="outer-text">
            <h2>Join Us</h2>
            <p>
                <ul class="social">
                <li><a href="<?php echo ot_get_option('krs_fb_sn'); ?>" class="icn-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo ot_get_option('krs_tweet_sn'); ?>" class="icn-twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo ot_get_option('krs_instagram_sn'); ?>" class="icn-instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </p>
            <p>
                P: <?php echo ot_get_option('krs_phone'); ?>
            </p>
            <p>
                E: <?php echo ot_get_option('krs_email'); ?>
            </p>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>