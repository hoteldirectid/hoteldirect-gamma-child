<div class="section home-location">
    <div class="container">
    <div class="title-section">
        <h2><?php the_sub_field('title'); ?></h2>
        <h3><?php the_sub_field('title_2'); ?></h3>
    </div>
    <div class="outer-home-location">
        <div class="list-location">
        <div class="img">
            <div class="outer-img">
                <?php 

                $location = get_sub_field('maps');

                if( !empty($location) ):
                ?>
                <div class="acf-map">

                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="text">
            <div class="outer-text">
            <h2>Address</h2>
            <?php the_sub_field('address'); ?>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>