    <div class="section home-room">
      <div class="container">
        <div class="title-section">
          <h2><?php the_sub_field('title'); ?></h2>
          <h3><?php the_sub_field('title_2'); ?></h3>
        </div>
        <div class="outer-room-list">
            <?php $posts = get_sub_field('post');
            foreach ($posts as $post) : 
            ?>
                <div class="list-room">
                    <div class="img">
                    <div class="outer-img">
                        <?php 
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail();
                        } 
                        ?>
                    </div>
                    </div>
                    <div class="text">
                        <div class="outer-text">
                            <h2><?php the_title(); ?></h2>
                            <?php echo custom_field_excerpt($post->post_content, 40); ?>
                            <a href="<?php the_permalink(); ?>" class="btn-main">ROOM DETAILS</a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            <?php wp_reset_postdata(); ?>
        </div>
      </div>
    </div>