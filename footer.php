  <div class="mobmenu">
  <div id="mobinav" class="sidenav">
    <div class="mobinav-inner login">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
          <?php karisma_nav_top_mobile(); ?>

          <?php karisma_nav_mobile(); ?>

    </div>
  </div><!-- end .mobinav -->
  <span class="icon-list-add mobico" onclick="openNav()"></span>
</div><!-- end .mobmenu -->
<footer class="main-footer">
  <div class="footer-link">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="logo-footer">
            <img src="<?php echo ot_get_option('krs_logo'); ?>" alt="">
          </div>
          <div class="address-footer">
            <h3>ADDRESS</h3>
            <p>
              <?php echo ot_get_option('krs_address'); ?>
            </p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="social-footer text-right">
            <h3>FOLLOW US</h3>
            <?php krs_sn('text-right'); ?>
          </div>
        </div>
      </div>
    </div><!-- end  .container -->
  </div><!-- end .footer-content -->
  <div class="footer-info">
    <div class="container">
      <p class="verticent-inner"><?php krs_footer(); ?></p>
    </div>
  </div><!-- end .footer-info -->
</footer>
<?php wp_footer(); ?>
</body>
</html>