jQuery(document).ready(function () {    
    jQuery('#slider-main').owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 2500,
        navText: ["<i class='icon-left-open-big'></i>", "<i class='icon-right-open-big'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    jQuery('.slider-facilities').owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 2500,
        navText: ["<i class='icon-left-open-big'></i>", "<i class='icon-right-open-big'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
});