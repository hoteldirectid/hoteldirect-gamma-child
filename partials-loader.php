<?php
    get_template_part('partials/stripe', 'sliders');

    // are there any rows within within our flexible content?
    if( have_rows('section') ): 

    // loop through all the rows of flexible content
    while ( have_rows('section') ) : the_row();

            if( get_row_layout() == 'hero_image' ) {
                get_template_part('partials/stripe', 'hero');
            }

            if( get_row_layout() == 'list_posts' ) {
                get_template_part('partials/stripe', 'posts');
            }

            if( get_row_layout() == 'text_slider' ) {
                get_template_part('partials/stripe', 'text');
            }

            if( get_row_layout() == 'gallery_section' ) {
                get_template_part('partials/stripe', 'gallery');
            }

            if( get_row_layout() == 'testimonial_section' ) {
                get_template_part('partials/stripe', 'testimonial');
            }

            if( get_row_layout() == 'contact_section' ) {
                get_template_part('partials/stripe', 'contact');
            }

            if( get_row_layout() == 'contact_page_section' ) {
                get_template_part('partials/stripe', 'contactpage');
            }

            if( get_row_layout() == 'address_section' ) {
                get_template_part('partials/stripe', 'location');
            }


    endwhile; // close the loop of flexible content
    
    endif; // close flexible content conditional
?>