<?php get_header(); ?>
  <div id="wrapper" class="singlepage room-details">
    <div class="hero-image" style="background:url(<?php the_post_thumbnail_url(); ?>)">
    </div>
    <div class="section home-room">
      <div class="container">
        <!-- <div class="title-section">
          <h3>Deluxe Room</h3>
        </div> -->
        <div class="outer-room-list room-details">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <div class="list-room">
                    <div class="img">
                    <div class="outer-img">
                        <div class="section main-slider">
                        <div class="owl-carousel slider-facilities">
                            <?php 
                                $images = get_field('gallery');
                                foreach ($images as $image) {
                                    echo '<div class="owl-slide" style="background-image: url(\''.$image['url'].'\')"></div>';
                                }
                            ?>
                        </div><!-- end .slider-main -->
                        </div><!-- end .main-slider -->
                    </div>
                    </div>
                    <div class="text">
                    <div class="outer-text">
                        <h2><?php the_title(); ?></h2>
                        <?php echo custom_field_excerpt(get_the_content(), 40); ?>
                    </div>
                    </div>
                </div>
                <!-- room description -->
                <div class="room-description">
                    <ul>
                        <?php $room_details = get_field('room_details');
                        foreach ($room_details as $room_detail) : 
                        ?>
                            <li>
                                <h4><?php echo $room_detail['name']; ?></h4>
                                <p><?php echo $room_detail['details']; ?></p>
                            </li>           
                        <?php endforeach; ?>

                        <?php ?>
                    </ul>
                </div>
                <!-- end room description -->

            <?php endwhile; ?>
            <?php endif; ?>

          <div class="other-rooms">
              <?php krs_related_post(); ?>

          </div>
        </div>

      </div>
    </div>

  </div><!-- end #wrapper -->

  <?php get_footer(); ?>