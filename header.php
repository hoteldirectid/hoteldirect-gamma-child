<!DOCTYPE html>
<html lang="en">
<head>
<?php wp_head(); ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <header>
  <div class="nav-top">
    <div class="container">
          <?php karisma_nav_top(); ?>
    </div>
  </div>
  <div class="top-section">
    <div class="container">
      <div class="logo pull-left">
        <a href="<?php echo get_home_url(); ?>">
          <img src="<?php echo ot_get_option('krs_logo'); ?>" alt="<?php bloginfo( 'name' ); ?>">
        </a>
      </div>
      <div class="address pull-left">
        <h1><?php bloginfo( 'name' ); ?></h1>
        <h4><?php echo ot_get_option('krs_address'); ?> | P: <?php echo ot_get_option('krs_phone'); ?> | F: <?php echo ot_get_option('krs_fax'); ?></h4>
      </div>
    </div>
  </div>
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button> -->
        <button type="button" class="navbar-toggle" onclick="openNav()">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse pull-left">
          <?php karisma_nav(); ?>
      </div>
      <!-- mobile menu -->
    </div>
  </div>
</header>